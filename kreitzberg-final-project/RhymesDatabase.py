import sqlite3

class RhymesDatabase:
  def __getitem__(self, word):
    connection = sqlite3.connect("./data/rhymes.db")
    cursor = connection.cursor()

    cursor.execute("SELECT (word_id) FROM WORDS WHERE word=?", [word])
    results = cursor.fetchall()
    if len(results) == 0:
      return []
    word_id = results[0][0]

    cursor.execute("SELECT (rhyme) FROM RHYMES WHERE word_id=?", [word_id])
    results = cursor.fetchall()

    cursor.close()      
    connection.commit() 
    connection.close()
    return [rhyme[0] for rhyme in results]
