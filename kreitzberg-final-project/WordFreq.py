import sqlite3
import numpy as np

class WordFreq:
  def __init__(self):
    connection = sqlite3.connect("./data/shakespear-word-freq.db")
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM TOTALWORDS")
    results = cursor.fetchall()
    self.num_words = results[0][1]


    cursor.execute("SELECT * FROM WORDFREQ")
    self.words = cursor.fetchall()
    cursor.close()      
    connection.commit() 
    connection.close()
    self.words = sorted(self.words)
    self.words = self.words[1:]

    self.cumulative_prob_to_word = np.zeros(len(self.words))

    for i in range(len(self.words)):
      self.cumulative_prob_to_word[i] = self.words[i][2]
    self.max_cum_prob = self.words[-1][2]

  def get_random_word(self):
    j = np.random.randint(0, self.max_cum_prob)
    index = np.searchsorted(self.cumulative_prob_to_word, j)
    word = self.words[index][1]
    return word
