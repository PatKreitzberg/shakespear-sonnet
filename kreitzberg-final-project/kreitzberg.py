import sys
import numpy as np
from collections import defaultdict as dd
from RhymesDatabase import RhymesDatabase
from WordFreq import *

SHORTEST_LINE = 5
LONGEST_LINE = 11
NUMBER_LINES = 14
punctuation = ",.;:?!'"

def clean_word(word):
  for p in punctuation:
    word = word.replace(p,'')
  return word


# can do which words start sentence most, then go from there

def generate_line_from_word_vec(random_word_by_word_freq, gen_last_word, num_words, word_vector):
  word = random_word_by_word_freq.get_random_word()
  line_len = np.random.randint(SHORTEST_LINE, LONGEST_LINE + gen_last_word)
  line = [word]

  for i in range(line_len-1):
    if len(word_vector[line[-1]]) == 0:
      line.append(random_word_by_word_freq.get_random_word())
      continue
        
    j = np.random.randint(0, len(word_vector[line[-1]]))
    word = list(word_vector[line[-1]])[j]
    line.append(word)
  return line


def adjust_line_ending(line, rhyme_database):
  rhymes = rhyme_database[line[-1]]
  i = -1
  while len(rhymes) == 0 and i < len(line):
    i += 1
    rhymes = rhyme_database[line[i]]

  line[-1], line[i] = line[i], line[-1]
  return line, rhymes

def get_unique_rhyme(word, rhymes):
  rhyme_word = rhymes[np.random.randint(0, len(rhymes))]
  if rhyme_word.lower() == word:
    i = 0
    while i < len(rhymes) and rhyme_word.lower() == word:
      rhyme_word = rhymes[i]
      i += 1
  return rhyme_word.lower()

def get_two_rhyming_lines(random_word_by_word_freq, rhyme_database, num_words, word_vector):
  first_line_array         = generate_line_from_word_vec(random_word_by_word_freq, True, num_words, word_vector)
  first_line_array, rhymes = adjust_line_ending(first_line_array, rhyme_database)
  second_line_array        = generate_line_from_word_vec(random_word_by_word_freq, False, num_words, word_vector)

  rhyming_word = get_unique_rhyme(first_line_array[-1], rhymes)
  second_line_array.append(rhyming_word)

  first_char = first_line_array[0][0].upper()
  first_line_array[0] = first_char + first_line_array[0][1:]

  first_char = second_line_array[0][0].upper()
  second_line_array[0] = first_char + second_line_array[0][1:]

  first_line = ''
  second_line = ''
  for a in first_line_array:
    first_line += a + ' '
  for b in second_line_array:    
    second_line += b + ' '    
  return first_line, second_line

def intertwine_lines(lines, A_lines, B_lines):
  for A,B in zip(A_lines, B_lines):
    lines.append(A)
    lines.append(B)
  return lines

def generate_word_vectors():
  text = './data/shakespear.txt'
  word_vector = dd(set)
  with open(text, 'r+') as f:
    for line in f:
      ls = line.split()
      for i in range(len(ls)-1):
        word = ls[i]
        word = clean_word(word)
        next_word = clean_word(ls[i+1])
        word_vector[word].add(next_word.lower())

  #IF ALL UPPERCASE THEN DON'T DO BECAUSE IT IS A NAME
  return word_vector

def generate_from_words_to_frequency(n):
  # fixme: if ends in hyphenated word, rhyme with second word in hyphenated
  rhyme_database = RhymesDatabase()
  word_vector = generate_word_vectors()
  num_words = len(word_vector)
  random_word_by_word_freq = WordFreq()
  for i in range(n):
    # Generate output
    A_lines = get_two_rhyming_lines(random_word_by_word_freq, rhyme_database, num_words, word_vector)
    B_lines = get_two_rhyming_lines(random_word_by_word_freq, rhyme_database, num_words, word_vector)
    C_lines = get_two_rhyming_lines(random_word_by_word_freq, rhyme_database, num_words, word_vector)
    D_lines = get_two_rhyming_lines(random_word_by_word_freq, rhyme_database, num_words, word_vector)
    E_lines = get_two_rhyming_lines(random_word_by_word_freq, rhyme_database, num_words, word_vector)
    F_lines = get_two_rhyming_lines(random_word_by_word_freq, rhyme_database, num_words, word_vector)
    G_lines = get_two_rhyming_lines(random_word_by_word_freq, rhyme_database, num_words, word_vector)
    
    lines = intertwine_lines([], A_lines, B_lines)
    lines = intertwine_lines(lines, C_lines, D_lines)
    lines = intertwine_lines(lines, E_lines, F_lines)
    lines += G_lines
    
    for line in lines:
      print(line)
    print()
    print()    

def main(args):
  text, n = None, None
  try:
    n = int(args[0])
  except:
    print('usage: <number sonnects>')
    exit(1)
    
  generate_from_words_to_frequency(n)
  print()
  
if __name__=='__main__':
  main(sys.argv[1:])

