# SUPERVISED LEARNING PROBLEM
# Table of data- last column missing (part of speech for word i)
# Find correlations from other columns to predict last one

# COLUMNS
# Part of speech previous word  Last 3 letters of word i+1

# WEIGHTS
# Dictionary of dictionary
# Associates feature-class pairs with weights

# AVERAGING WEIGHTS
# Keep track of when weight was last updated

# FEATURES
# self.weights["last word ends in ing"] = {"VERB":0.75, "NOUN":0.1, "PROPN":0.05}
# self.weights["last word ends in ed"] = {"VERB":0.1, "NOUN":0.7, "PROPN":0.2}

from collections import defaultdict as dd
from tokenized_sonnets import *
import re
import sqlite3

punctuation = ',.?!:;'

class Tagger:
  def __init__(self):
    self.data = dd(lambda : {})
    self.end_of_sentence_markers = ['\n']
    self.weights = dd(lambda: dd(float))
    self.total_evals = dd(lambda: dd(float))
    self.last_iteration_weight_changed = dd(lambda: dd(int))
    self.classes = set()
    for word, tag in word_to_tag.items():
      self.classes.add(tag)
    print('classes', list(self.classes))
    self.initialize_weights()
    print('training...')
    self.train()
    print('done training')
    self.avg_weights()


  def initialize_weights(self):
    for sonnet in sonnets:
      for line in sonnet:
        words = re.split('[\s+-]', line)
        for i in range(len(words)):
          word = self.clean_word(words[i])
          tag = word_to_tag[word]
          features = self.get_features(word, i, words)
          for feature in features:
            self.weights[feature][tag] += 1.0

  def get_tag_from_dict(self, word):
    if word in word_to_tag:
      return word_to_tag[word]
    # else returns None
    
  def get_features(self, word, i, words):
    tag_i_neg_1, tag_i_neg_2 = None, None
    clean_word_i_neg_1, clean_word_i_neg_2 = None, None
    if i > 0:
      clean_word_i_neg_1 = self.clean_word(words[i-1])
      tag_i_neg_1 = self.get_tag_from_dict(clean_word_i_neg_1)
    if i > 1:
      clean_word_i_neg_2 = self.clean_word(words[i-2])
      tag_i_neg_2 = self.get_tag_from_dict(clean_word_i_neg_2)
    return self.get_features_of_word_from_sentence(i, word, words, tag_i_neg_1, tag_i_neg_2, clean_word_i_neg_1, clean_word_i_neg_2)

  def clean_word(self, word):
    word = word.lower()
    if len(word) > 0 and word[-2:] == "'s":
      word = word[:-2]
    if len(word) > 0 and word[0] == "'":
      word = word[1:]
    if len(word) > 0 and word[-1] == "'":
      word = word[:-1]
    for p in punctuation:
      word = word.replace(p,"")
    return word.lower()

  def predict(self, features):
    '''
    Dot-product features with current weights.
    Return class with max score
    '''
    scores_per_class = dd(float)
    for feature in features:
      if feature in self.weights:
        weights_per_class = self.weights[feature]
        for _class, weight in weights_per_class.items():
          scores_per_class[_class] += weight
    if len(scores_per_class) == 0:
      return

    best_class_fit, score = max(scores_per_class.items(), key=lambda x:x[1])
    return best_class_fit

  def get_features_of_word_from_sentence(self, i, word, sentence, prev_tag, prev2_tag, prev_word, prev2_word):
    def add(name, *args):
      if len(args) == 1 and args[0] is not None:
        features.add('+'.join((name,) + tuple(args)))
      if len(args) == 2 and args[0] is not None and args[1] is not None:
        features.add('+'.join((name,) + tuple(args)))
    
    features = set()
    add('bias') # This acts sort of like a prior
    add('i suffix', word[-3:])
    add('i pref1', word[0])
    add('i-1 tag', prev_tag)
    add('i-2 tag', prev2_tag)
    add('i tag+i-2 tag', prev_tag, prev2_tag)
    add('i word', word)
    add('i-1 tag+i word', prev_tag, word)

    if i > 0:
      add('i-1 word', prev_word)
      add('i-1 suffix', prev_word[-3:])

    if i > 1:
      add('i-2 word', prev2_word)

    if i < len(sentence) - 1:
      add('i+1 word', self.clean_word(sentence[i+1]))
      add('i+1 suffix', self.clean_word(sentence[i+1][-3:]))

    if i < len(sentence) - 2:
      add('i+2 word', self.clean_word(sentence[i+2]))
    return features

  def update_weight(self, correct_class, guess_class, features, current_iteration):
    if correct_class == guess_class:
      return

    for feature in features:
      for _class, value in [(correct_class, 1.0), (guess_class, -1.0)]:
        number_of_iterations_at_this_weight = 1 + current_iteration - self.last_iteration_weight_changed[feature][_class]
        self.last_iteration_weight_changed[feature][_class] = current_iteration
        self.total_evals[feature][_class] = number_of_iterations_at_this_weight * self.weights[feature][_class]
        self.weights[feature][_class] += value

  def train(self):
    number_training_terations = 1000
    for i in range(number_training_terations):
      for sonnet in sonnets:
        for line in sonnet:
          words = re.split('[\s+-]', line)
          for i in range(len(words)):
            word     = self.clean_word(words[i])
            tag      = word_to_tag[word]
            features = self.get_features(word, i, words)
            guess    = self.predict(features)
            if guess is None:
              continue
            self.update_weight(tag, guess, features, i)

  def tag_sentence(self, sentence, word_to_tag_to_count):
    words = re.split('[\s+-]', sentence)
    for i in range(len(words)):
      word = self.clean_word(words[i])
      if len(word) < 1:
        continue
      features = self.get_features(word, i, words)            
      tag    = self.predict(features)
      word_to_tag_to_count[word][tag] += 1.0
    return word_to_tag_to_count

  def tag_text(self, fname):
    word_to_tag_to_count = dd(lambda: dd(float))
    sentence_endings = '.?!;'
    sentence = ''
    line_count = 0

    with open(fname, 'r') as f:
      for line in f:
        line_count += 1
        if line_count % 1000 == 0:
          print(line_count/1000)
        ending_poses = []
        for ending in sentence_endings:
          if line.find(ending) != -1:
            ending_poses.append(line.find(ending))
        if len(ending_poses) > 0:
          sentence += line[:min(ending_poses)]
          self.tag_sentence(sentence, word_to_tag_to_count)
          sentence = line[min(ending_poses)+1:]
        else:
          sentence += ' ' + line
      f.close()
              
    # build tables
    import os
    database = './data/shakespear-tagged-words.db'
    os.system('rm ' + database)
    connection = sqlite3.connect(database)
    cursor = connection.cursor()
    cursor.execute("CREATE TABLE WORDTAGS (word_id int not null, word var(255) not null, tag var(255) not null)")

    word_id = 0
    for word in word_to_tag_to_count:
      tag, count = max(word_to_tag_to_count[word].items(), key=lambda x:x[1])
      if tag is not None:
        print(word, tag)
        cursor.execute("INSERT INTO WORDTAGS (word_id, word, tag) VALUES (?,?,?)", [word_id, word, tag])
        word_id += 1
    cursor.close()
    # save changes to file for next exercise
    connection.commit()
    connection.close()


  def avg_weights(self):
    for feat in self.weights:
      for _class in self.weights[feat]:
        if self.total_evals[feat][_class] == 0:
          # guess was always correct for this feature/class pair
          continue
        self.weights[feat][_class] = self.weights[feat][_class]/self.total_evals[feat][_class]

  def test(self):    
    right = 0
    wrong = 0
    with open('./data/shakespear-sonnets.txt', 'r') as f:
      for line in f:
        if line == '\n' or len(line) < 2:
          continue
        words = re.split('[\s+-]', line)
        for i in range(len(words)):
          word = self.clean_word(words[i])
          if len(word) < 1:
            continue
          features = self.get_features(word, i, words)            
          guess    = self.predict(features)
          #print(word, '\t', guess, '\t', tag)
          if word in word_to_tag:
            tag      = word_to_tag[word]
            if tag == guess:
              right += 1
            else:
              wrong += 1
      f.close()
    print(right, wrong)                

  # def compare_against_spacy(self):
  #   # fime: remove before submission
  #   import spacy 
  #   right, wrong = 0, 0
  #   nlp = spacy.load("en_core_web_sm")
  #   output = []
  #   with open('./data/shakespear-sonnets.txt') as f:
  #     for line in f:
  #       if line == '\n' or len(line) < 2:
  #         continue
  # 
  #       spacy_word_tag_pairs = {}
  #       doc = nlp(line)
  #       for token in doc:
  #         spacy_word_tag_pairs[str(token).lower()] = token.pos_
  #       words = re.split('[\s+-]', line)
  # 
  #       for i in range(len(words)):
  #         word = self.clean_word(words[i])
  #         if word not in spacy_word_tag_pairs:
  #           continue
  #         if len(word) < 1:
  #           continue
  #         features = self.get_features(word, i, words)            
  #         guess    = self.predict(features)
  #         spacy_tag = spacy_word_tag_pairs[word]
  #         #print(word, '\t', guess, '\t', tag)
  #         if guess == spacy_tag:
  #           right += 1
  #         else:
  #           wrong += 1
  #     f.close()
  #   print(right, wrong)                
      
tagger = Tagger()
#tagger.tag_text('./data/shakespear.txt')
